public with sharing class objsave
{
    public Account acc {get;set;}
    public Account myacc {get;set;}
    private ApexPages.StandardController controller {get;set;}
    List<Account> myacclist = new List<Account>();
    public objsave(ApexPages.StandardController controller) {
        myacc = new Account();
        this.controller = controller;
        acc = (Account)controller.getRecord();
        myacc.Name= acc.Name;
        myacc.AnnualRevenue= acc.AnnualRevenue;
        myacc.Type= acc.Type;
        myacc.Industry= acc.Industry;
        myacc.Description= acc.Description;
        myacc.Phone= acc.Phone;
        myacc.Sic= acc.Sic;
        myacc.Fax= acc.Fax;
        myacc.Website= acc.Website;
        
        
        myacclist.add(myacc);
    }
    
    public PageReference submit()
    {
        insert myacclist;
        return controller.save();
    }
}