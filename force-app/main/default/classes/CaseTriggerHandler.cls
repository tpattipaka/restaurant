/********************************************
Class Name: CaseTriggerHandler
Test Class: CaseTriggerTest
Purpose: Handler class for CaseTrigger
********************************************/

public with sharing class CaseTriggerHandler {

    public static Map<String, String> slaMap = new Map<String, String>{'Platinum' => 'High', 'Gold' => 'Medium', 'Silver' => 'Low'};
    public static Map<String, Integer> priorityMap = new Map<String, Integer>{'High' => 2, 'Medium' => 4, 'Low' => 7};

    public static void setCasePriority(List<Case> newCases) {
        Set<Id> conIdSet = new Set<Id>();
        if(!newCases.isEmpty()) {
            for(Case newCase : newCases) {
                conIdSet.add(newCase.ContactId);
            }

            Map<Id, Contact> conMap = new Map<Id, Contact>([SELECT Id, Account.SLA__c FROM Contact WHERE Id IN :conIdSet]);

            for(Case newCase : newCases) {
                if(newCase.ContactId != null) {
                    newCase.Priority = slaMap.get(conMap.get(newCase.ContactId).Account.SLA__c);
                }
            }
        }
    }

    //Create an Event for every new Case that gets created.
    public static void createVisits(List<Case> newCases) {
        List<Event> visitList = new List<Event>();
        if(!newCases.isEmpty()) {
            for(Case newCase : newCases) {
                Event newVisit = new Event();
                newVisit.WhatId = newCase.Id;
                newVisit.Subject = newCase.Subject;
                newVisit.StartDateTime = System.now().addDays(priorityMap.get(newCase.Priority));
                newVisit.EndDateTime = newVisit.StartDateTime.addHours(2);
                visitList.add(newVisit);
            }
        }
        if(!visitList.isEmpty()) {
            Database.SaveResult[] srList = Database.insert(visitList, false);
            for(Database.SaveResult sr : srList) {
                if(!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        ExceptionHandler.handleDatabaseErrors('CaseTriggerHandler', 'createVisits', err);
                    }
                }
            }
        }
    }
}