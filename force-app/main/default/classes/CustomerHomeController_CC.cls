public class CustomerHomeController_CC {

    public PageReference RecCodes() {
        return null;
    }


    public String ipText{get;set;}
    public User UserList{get; set;}
    public String selectString{get; set;}
    public List<Id> userIdList{get; set;}
    public List<SelectOption> options;
    public List<Customer__c> customerList = new List<Customer__c>();

    public CustomerHomeController_CC() {
        customerList = [SELECT Id, Location__c,Location__r.Reg_Code__c,Location__r.User__c,Location__r.User__r.Name FROM Customer__c];
    }
    
    public List<SelectOption> getLocUsers() {
         options = new List<SelectOption>();
        for(Customer__c c : customerList) {
            options.add(new SelectOption(c.Location__r.User__c,c.Location__r.User__r.Name) );
        }
        return options;
    }
    
    public List<SelectOption> getRecCodes() {
         options = new List<SelectOption>();
        for(Customer__c c : customerList) {
            options.add(new SelectOption(c.Location__r.Reg_Code__c,c.Location__r.Reg_Code__c) );
        }
        return options;
    }
}