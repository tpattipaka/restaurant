public with sharing class buttonController {

private ApexPages.StandardController standardController;

    public buttonController(ApexPages.StandardController StandardController) {
    
    this.StandardController = StandardController;
    }
    
    public PageReference Proceed()
    {
    PageReference pr = new PageReference('/apex/alertspage');
    return pr;
    }

}