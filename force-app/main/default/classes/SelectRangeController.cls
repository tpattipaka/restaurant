public with sharing class SelectRangeController {
    
    List<Account> accList = new List<Account>();
    
    public SelectRangeController(ApexPages.StandardController controller) {
    
    }
    
    public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [Select Id,Name,BillingCity,BillingCountry,Location__Latitude__s,Location__Longitude__s from Account where Location__Latitude__s != Null]));
            }
            return setCon;
        }
        set;
    }

    // Initialize setCon and return a list of records
    public List<Account> getAccounts() {
        return (List<Account>) setCon.getRecords();
    }
      
}