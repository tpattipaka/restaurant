public class batchClass2 implements database.batchable<Sobject> 
{
    
    public string query = 'select id from account';
    public integer count = 5;
    
public database.querylocator start(database.BatchableContext bc)  
{
    return database.getQueryLocator(query);
}

public void execute(database.BatchableContext bc, list<Location__c> scope)
{
    list<Location__c> listLocation = new list<Location__c>();
    Location__c obj1;
    integer i;
    if(count!=0)
    {
        for(i=0;i<200;i++)
        {
            obj1 = new Location__c(Name = 'Loc'+string.valueOf(i), Region__c='Eastern');
            listLocation.add(obj1);           
        }
        count--;           
    }
    update listLocation;
}

public void finish(database.BatchableContext bc)
{
   
}    
}