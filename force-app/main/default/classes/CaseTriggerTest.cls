/**********************************************************
Class Name: CaseTriggerTest
Purpose: Tets class for CaseTrigger and CaseTriggerHandler
**********************************************************/

@isTest
public with sharing class CaseTriggerTest {

    //Initialize test data from TestDataFactory.cls
    private static void initializeTestData() {
        List<Account> accList = TestDataFactory.createAccounts(3);
        List<Contact> conList = new List<Contact>();
        List<Case> caseList = new List<Case>();
        Id fsQueueId = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperNAME = 'Field_Service_Queue' LIMIT 1].Id;
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User testUser = TestDataFactory.createUser('test.user@hfx.com', profileId);

        for(Account acc : accList) {
            conList.addAll(TestDataFactory.createTestContacts(2, acc.Id));
        }
        for(Contact con : conList) {
            caseList.addAll(TestDataFactory.createTestCases(2, con.AccountId, con.Id));
        }
        for(Case c : caseList) {
            c.OwnerId = fsQueueId;
        }
        update caseList;
    }

    @isTest
    static void testCasePriority() {
        initializeTestData();
        List<Case> caseList = [SELECT Id, ContactId, Priority FROM Case];
        Integer eventCount = [SELECT Count() FROM Event];
        CaseTriggerHandler.setCasePriority(caseList);
        System.assertEquals('Medium', caseList[0].Priority);
        System.assertEquals(eventCount, caseList.size());
    }
    
}