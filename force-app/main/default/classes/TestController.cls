public class TestController{ 

public apexpages.standardController controller {get; set; }
public Account a;
public decimal lat;
public decimal lon;
public List<Account> searchResults {get; set;}
public string searchText{
    get{
    if (searchText==null) searchText = '';
    return searchText;
    }
    set;
}

public TestController(ApexPages.StandardController controller){
    this.controller = controller;
    controller.addFields(new List<String>{'Name','Location__Latitude__s','Location__Longitude__s','BillingCity','BillingStreet','BillingCountry'});
    this.a = (Account)controller.getRecord();
    this.lat = a.Location__Latitude__s;
    this.lon = a.Location__Longitude__s;
    
}

public PageReference search(){
    if(SearchResults == null){
        SearchResults = new List<Account>();
    }
    else{
        SearchResults.Clear();
    }
    system.debug('#lat: '+a.Location__Latitude__s);
    system.debug('#lon: '+a.Location__Longitude__s);
    String qry = 'SELECT ID, Name,BillingStreet,BillingCity,BillingCountry, Location__Latitude__s, Location__Longitude__s FROM Account WHERE DISTANCE(Location__c, GEOLOCATION('+lat+', '+lon+'), \'mi\')<'+searchText;
    SearchResults = Database.query(qry);
    return null;
}

}