@isTest
public class AccountManagerTest {
    static testMethod void getAccountTest(){
        Id recordId = createTestRecord();
        RestRequest request = new RestRequest();
        request.httpMethod = 'GET';
        request.requestURI = 'https://yourInstance.salesforce.com/services/apexrest/Accounts/contacts/'+recordId;
        RestContext.request = request;
        Account testAccount = AccountManager.getAccount();
        String contactName = [Select Id,Name from Contact where AccountId =: recordId].Name;
        system.assertEquals('first last', contactName);
    }
    
    static Id createTestRecord() {
        // Create test records
        Account acc = new Account(Name = 'test acc');
        insert acc;
        Contact con = new Contact(AccountId=acc.Id);
            con.FirstName = 'first';
            con.LastName = 'last';
        insert con;
        return acc.Id;
    }        
}