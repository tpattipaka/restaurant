public class LocationCallouts {

     @future (callout=true)  // future method needed to run callouts from Triggers
      static public void getLocation(id accountId){
      Account a = [SELECT BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet,Location__Latitude__s,Location__Longitude__s FROM Account WHERE id =: accountId];
         String address = '';
        if (a.BillingStreet != null)
            address += a.BillingStreet +', ';
        if (a.BillingCity != null)
            address += a.BillingCity +', ';
        if (a.BillingState != null)
            address += a.BillingState +', ';
        if (a.BillingPostalCode != null)
            address += a.BillingPostalCode +', ';
        if (a.BillingCountry != null)
            address += a.BillingCountry;
        address = EncodingUtil.urlEncode(address, 'UTF-8');
          system.debug('$'+address);
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://maps.googleapis.com/maps/api/geocode/json?address='+address+'&sensor=false');
        req.setMethod('GET');
        req.setTimeout(60000);

        try{

            HttpResponse res = h.send(req);
            JSONParser parser = JSON.createParser(res.getBody());
            double lat = null;
            double lon = null;            
            while (parser.nextToken() != null) {
                system.debug('*'+parser.getText());
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                    (parser.getText() == 'location')){
                       parser.nextToken(); // object start
                       while (parser.nextToken() != JSONToken.END_OBJECT){
                           String txt = parser.getText();
                           parser.nextToken();
                           if (txt == 'lat'){
                               system.debug('*Lat');
                               lat = parser.getDoubleValue();
                           }
                           else if (txt == 'lng'){
                               system.debug('*Lon');
                               lon = parser.getDoubleValue();
                           }
                       }
                }
            }
            if (lat != null){
                a.Location__Latitude__s = lat;
                a.Location__Longitude__s = lon;
                update a;
                            }
                } catch (Exception e) {
        }  
    }
}