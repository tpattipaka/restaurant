trigger AccountTrigger on Account (before insert, after insert, before update, after update) {
    
    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            // Process before insert
        } else if (Trigger.isAfter) {
            //AccountTriggerHandler.afterInsert(Trigger.new);
        }
    } else if (Trigger.isUpdate) {
        if (Trigger.isBefore) {
            // Process before update
        } else if (Trigger.isAfter) {
            // Process after update
        }
    }

}