/**********************************************************
Trigger Name: CaseTrigger
Test Class: CaseTriggerTest
Purpose: To perform custom logic for DML operations on Cases
************************************************************/

trigger CaseTrigger on Case (before insert, after insert, before update, after update) {

    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            CaseTriggerHandler.setCasePriority(Trigger.new);
        } if (Trigger.isAfter) {
            CaseTriggerHandler.createVisits(Trigger.new);
        }
    } else if (Trigger.isUpdate) {
        if (Trigger.isBefore) {
            // Before update logic
        } else if (Trigger.isAfter) {
            // After update logic
        }
    }

}