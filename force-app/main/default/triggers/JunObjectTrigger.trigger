trigger JunObjectTrigger on JunObject__c (before insert, before update) {
    Integer jCount = 0;
    for(JunObject__c j: Trigger.new) {
        jCount = [SELECT count() from JunObject__c WHERE ObjectA__c=:j.ObjectA__c AND ObjectB__c=:j.ObjectB__c];
        if(jCount != 0){
            j.addError('This combination already exists');
        }
    }

}