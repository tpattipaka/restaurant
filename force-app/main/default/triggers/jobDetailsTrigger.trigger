trigger jobDetailsTrigger on Consolidated_Line__c (before insert) {
    Invoice__c invoice = new Invoice__c();
    Placement__c placement = new Placement__c();
    Job__c job = new Job__c();
    Fee__c fee = new Fee__c();
    Invoice_Item__c invoiceItem = new Invoice_Item__c();
    for(Consolidated_Line__c con : Trigger.new) {  
        invoice = [SELECT Id,(SELECT Id, Fee__c, Invoice__c FROM Invoice_Items__r), Placement__r.Job__c, Placement__c 
                   FROM Invoice__c WHERE Id =: con.Invoice__c];
        job = [SELECT Id, NumberField__c, TextField__c FROM Job__c WHERE Id = : Invoice.Placement__r.Job__c];         
       /* placement = [SELECT Id, Job__c FROM Placement__c WHERE Id =: con.invoice__r.Placement__c];
        
        if(placement != null){
            job = [SELECT Id, NumberField__c, TextField__c FROM Job__c WHERE Id = : placement.Job__c];
        } */
        if (job == null) {
            invoiceItem = invoice.Invoice_Items__r[0];
            fee = [SELECT Id, Job__c from Fee__c WHERE Id =: invoiceItem.Fee__c];
            job = [SELECT Id, NumberField__c, TextField__c FROM Job__c WHERE Id = :invoiceItem.Fee__r.Job__c];
        } 
        con.TextField__c = job.TextField__c;
        con.NumberField__c = job.NumberField__c;
    }
}