import { LightningElement } from 'lwc';

export default class HelloWorld extends LightningElement {
  greeting = 'Naren';
  changeHandler(event) {
    this.greeting = event.target.value;
  }
}